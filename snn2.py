import numpy as np

x = np.array([[1, 0 , 1, 0], [0, 0 , 1, 0], [0, 0 , 0, 1], [1, 1 , 1, 1]])
y = np.array([1, 0, 0, 1])

W1 = np.random.rand(x.shape[1], 4)
W2 = np.random.rand(4, 1)

# activation function sigmoid 
def sigmoid(x, derivative=False):
    if derivative:
        sigm  = x * (1. - x)
    else:
        sigm = 1. / (1. + np.exp(-x))
    return sigm

def train():
    l1 = sigmoid(np.dot(x, W1))
    l2 = sigmoid(np.dot(l1, W2))

    l2_error = l2 - y
    l2_deldcta = *sigmoid(l2_error, derivative=True)
   

train()