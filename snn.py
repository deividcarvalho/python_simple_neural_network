import numpy as np

def sigmoid(x):
    return 1/1+np.exp(-x)

x = np.array([1, 2, 4])
y = [2, 4, 5]

w = .65
b = .85

epoch = 1

for i in range(epoch):

    l1 = x * w + b

    cost = (l1 - y) ** 2
    total_cost = np.sum(cost) / x.size

    dcdw = 2 * total_cost

    print(l1)
    print(cost)
    print(total_cost)